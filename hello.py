from flask import Flask, jsonify
from flask import Flask
from elasticsearch import Elasticsearch
import certifi
from datetime import datetime
from flask_cors import CORS, cross_origin
import math
from flask import request
from filters import *
from listing import *
import pymongo 
import datetime 
from mongo_collection import * 
from bson.objectid import ObjectId


app = Flask(__name__)
app.config.from_object('config')
CORS(app, support_credentials=True)
#client = pymongo.MongoClient("mongodb://172.17.0.1:27018")
#search_queries = client["search"] 
#search_queries.authenticate('search', 'search')
#collecton_queries = search_queries['queries']


@app.route('/search/api/v1.0/product/<string:search_query>/<int:frm>/<int:size>', methods=['GET'])

def get_search(search_query,frm,size):

    # you can use RFC-1738 to specify the url
    es = Elasticsearch([app.config['ELASTIC_ENDPOINT']])
    
    if(size > 100):
        return 'Asking for too big dataset'+str(size)
   
    query_dsl_string = {
   
        "query": { 
            "bool": { 
                "must": {
                    "match": {"proname" : search_query}
                },
                
                "filter": [ ]         
            }
        }
       
    }

    query_dsl_string["aggs"] = {
            "max_price" : { "max" : { "field" : "discountprice" } },
            "min_price" : { "min" : { "field" : "discountprice" } },
            "max_discount" : { "max" : { "field" : "discpercent" } },
            "min_discount" : { "min" : { "field" : "discpercent" } }
            
    }
    

    

    itemtype = request.args.get('itemtype')
    if(filters.AddMainCatFilters(itemtype)):
        query_dsl_string['query']['bool']['filter'].append(filters.AddMainCatFilters(itemtype))
    
    only_filter = request.args.get('filters')

    if not only_filter :

        query_dsl_string["from"]=frm
        query_dsl_string["size"] = size
        #AdditionalFilters
        min_price = request.args.get('rmin')
        max_price = request.args.get('rmax')
        subcat = request.args.get('scat')
        brandid = request.args.get('brandid')
        isavailable = request.args.get('isavailable')
        min_perc = request.args.get('min_perc')
        max_perc = request.args.get('max_perc')

        orderby = request.args.get('orderby')
        if orderby:
            ordertype = request.args.get('ordertype')
            query_dsl_string["sort"] = {
                orderby: {"order": ordertype}
            }



        if(request.args.get('user')):
            user = request.args.get('user')
        else:
            user = "Anonymous"

        
        if (filters.AddPercFilters(min_perc,max_perc)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddPercFilters(min_perc,max_perc))


        if(filters.AddPriceFilters(min_price,max_price)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddPriceFilters(min_price,max_price))
        
        
        if (filters.AddSubCatFilters(subcat)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddSubCatFilters(subcat))
        
        if (filters.AddBrandFilter(brandid)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddBrandFilter(brandid))


        if (filters.AddIsAvailableFilter(isavailable)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddIsAvailableFilter(isavailable))

    else:
        query_dsl_string["size"] = 5000
       
        query_dsl_string["_source"] =  ["third_catname","third_catid","brandname","brandid","second_catname","second_catid","first_catname","first_catid"]
       
    
    #return query_dsl_string
    #THE GREAT ELASTIC CALL    
    
    res = es.search(index="products-index", body=query_dsl_string)
    
    #return res
    savedrec = None
    if not only_filter :
        ##savedrec = collecton_queries.insert_one({ "keywords":search_query, "query":query_dsl_string, "date": "", "user":user, "done":"no","frm":frm})
        ##print(str(savedrec.inserted_id))

        #return False

        data = filters.Get_Results(res) 
        ##collecton_queries.update_one({'_id': savedrec.inserted_id}, {'$set':{'total':res['hits']['total']['value']}}, upsert=False)
        #print(str(savedrec.inserted_id))
    
    else:
        data = filters.Get_Filters(res)
        

    resp = {}
    resp['DSL'] = query_dsl_string
    if savedrec is not None:
        resp['search_log_id'] = str(savedrec.inserted_id)
    resp['data'] = data
    resp['total'] = res['hits']['total']['value']
    resp['start'] = frm
    resp['size'] = size
    resp['min_amt'] = res['aggregations']['min_price']['value']
    resp['max_amt'] = res['aggregations']['max_price']['value']
    resp['min_discperc'] = res['aggregations']['min_discount']['value']
    resp['max_discperc'] = res['aggregations']['max_discount']['value']
    return resp

#######################LISTING API###########################

@app.route('/listing/api/v1.0/<int:itemtype>/<string:listing_type>/<int:frm>/<int:size>/<int:catid>', methods=['GET'])

def get_product_listing(itemtype,listing_type,frm,size,catid):
    min_price = request.args.get('rmin')
    max_price = request.args.get('rmax')
    brandid = request.args.get('brandid')
    isavailable = request.args.get('isavailable')
    min_perc = request.args.get('min_perc')
    max_perc = request.args.get('max_perc')
    orderby = request.args.get('orderby')
   
    #Primary query
    query_dsl_string = listing.get_listing(itemtype,listing_type,frm,size,catid) 

    only_filter = request.args.get('filters')

    if not only_filter :

        if orderby:
            ordertype = request.args.get('ordertype')
            query_dsl_string["sort"] = {
                orderby: {"order": ordertype}
            }

        if (filters.AddPercFilters(min_perc,max_perc)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddPercFilters(min_perc,max_perc))


        if(filters.AddPriceFilters(min_price,max_price)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddPriceFilters(min_price,max_price))

        if (filters.AddBrandFilter(brandid)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddBrandFilter(brandid))


        if (filters.AddIsAvailableFilter(isavailable)):
            query_dsl_string['query']['bool']['filter'].append(filters.AddIsAvailableFilter(isavailable))

    else:
        query_dsl_string["size"] = 500
       
        query_dsl_string["_source"] =  ["third_catname","third_catid","brandname","brandid","second_catname","second_catid","first_catname","first_catid"]
       
       
           
    
    #return query_dsl_string
    

    

    es = Elasticsearch([app.config['ELASTIC_ENDPOINT']])
    res = es.search(index="products-index", body=query_dsl_string)   
    if not only_filter :
        data = filters.Get_Results(res)  
    else:
        data = filters.Get_Filters(res)
    
    resp = {}
    resp['DSL'] = query_dsl_string
    resp['data'] = data
    resp['total'] = res['hits']['total']['value']
    resp['start'] = frm
    resp['size'] = size
    resp['min_amt'] = res['aggregations']['min_price']['value']
    resp['max_amt'] = res['aggregations']['max_price']['value']
    resp['min_discperc'] = res['aggregations']['min_discount']['value']
    resp['max_discperc'] = res['aggregations']['max_discount']['value']
    return resp
    

@app.route('/search/api/v1.0/user_query/<string:uid>/<int:query_limit>', methods=['GET'])

def get_user_search(uid,query_limit):
    
    datafilter = {} 
    if(uid is not '0'):
        datafilter['user'] = uid
      
    if(request.args.get('zero_result')):
        datafilter['total'] = 0
    else:
         datafilter['total'] = { '$gt': 1 }    

    #print(datafilter)
    ##queries = collecton_queries.find(datafilter).sort(u'total',pymongo.ASCENDING).limit(query_limit)

    data = {}
    for i in queries:
        search_index = str(i['_id'])
        del i["_id"]
        data[search_index] = i

    return data


@app.route('/search/api/v1.0/user_click/<string:uid>/<int:page>/<int:proid>', methods=['GET'])

def get_user_click(uid,page,proid):
    ##seach_activity = collecton_queries.update_one({'_id': _byid(uid)}, {'$set':{'page':page,'proid':proid}}, upsert=False)
    return {'saved':'true'}

@app.route('/search/api/v1.0/user_clickthrough/<int:status>/<int:query_limit>', methods=['GET'])

def get_user_clickthrough(status,query_limit):
    datafilter = {} 
    if(status is not 0):
        datafilter['proid'] = { '$exists': True }
    else:
        datafilter['proid'] = { '$exists': False }     
    
    #print(datafilter)
    queries = collecton_queries.find(datafilter).limit(query_limit)

    data = {}
    for i in queries:
        search_index = str(i['_id'])
        del i["_id"]
        data[search_index] = i

    return data


def _byid(uid):
    return ObjectId(uid)

if (__name__ == "__main__"):
    app.run(host='0.0.0.0',port = 8000)
