from filters import *  
class listing:

    def get_listing(itemtype,listing_type,frm,size,catid):
        query_dsl_string = {

            "query": { 
                "bool": { 
                    "filter": [ ]
                }
            },

        }

        query_dsl_string["aggs"] = {
            "max_price" : { "max" : { "field" : "discountprice" } },
            "min_price" : { "min" : { "field" : "discountprice" } },
            "max_discount" : { "max" : { "field" : "discpercent" } },
            "min_discount" : { "min" : { "field" : "discpercent" } }
        }
        query_dsl_string['query']['bool']['filter'].append(filters.AddMainCatFilters(itemtype))
        
        query_dsl_string["from"]=frm
        query_dsl_string["size"] = size


        if(catid is not 0):
            query_dsl_string['query']['bool']['filter'].append(filters.AddSubCatFilters(catid))

            
        if listing_type ==   'featured_cat':
                featured =  {"match" :   {"categoryFeatured" : 'Yes' }}

        elif listing_type == 'featured_home':
                featured =  {"match" :   {"homeFeatured" : 'Yes' }}
        else:
             featured = None       

        if featured:
            query_dsl_string['query']['bool']['filter'].append(featured)

        return query_dsl_string
        
    