   
class filters:

    def AddPriceFilters(min_price,max_price):
        
        if (min_price) and (max_price) :
            return   {"range" :   {"discountprice" : { "gte": min_price, "lte": max_price,"boost" : 2.0 }}}     
        else:
            return  None

    def AddPercFilters(min_perc,max_perc):
        
        if (min_perc)  :
            return   {"range" :   {"discpercent" : { "gte": min_perc,"lte": max_perc, "boost" : 3.0 }}}     
        else:
            return  None

    def AddSubCatFilters(subcat):

        if (subcat):
            return   {"match" :   {"categoryPath" : subcat }}     
        else:
            return   None


    def AddMainCatFilters(itemtype):
        if (itemtype):
            return   {"match" :   {"first_catid" : itemtype }}     
        else:
            return   None


    def AddBrandFilter(brandid):
        if (brandid):
            brand_id = brandid.split(",")
            return   {"terms" :   {"brandid" : brand_id }}
        else:
            return   None

            

    def AddIsAvailableFilter(isavailable): 
        if (isavailable):
            return   {"match" :   {"isavailable" : isavailable }}     
        else:
            return   None

    def AddMainQuery(search_query):
        query_dsl_string = {
    
            "query": { 
                "bool": { 
                    "must": {
                        "match": {"proname" : search_query}
                    },
                    
                    "filter": [ ]
                }
            },
        
        }

        query_dsl_string["aggs"] = {
                "max_price" : { "max" : { "field" : "discountprice" } },
                "min_price" : { "min" : { "field" : "discountprice" } },
                "max_discount" : { "max" : { "field" : "discpercent" } },
                "min_discount" : { "min" : { "field" : "discpercent" } }
                
        }
        return query_dsl_string

    def Get_Filters(res):
        cat_list = list()
        brand_list = list()
        filter_data = {} 
        for hit in res['hits']['hits']:
            cat_list.append({'third_catid':hit['_source']['third_catid'],'third_catname':hit['_source']['third_catname'],'second_catname':hit['_source']['second_catname'],'second_catid':hit['_source']['second_catid'],'first_catname':hit['_source']['first_catname'],'first_catid':hit['_source']['first_catid']})
            
            brand_list.append({'id':hit['_source']['brandid'],'brandname':hit['_source']['brandname']})

        filter_data["unique_cat"] = list({v['third_catid']:v for v in cat_list}.values())
        filter_data["unique_brand"] = list({v['id']:v for v in brand_list}.values())
        
        return filter_data

    


    def Get_Results(res):
        arrayList = list()
        for hit in res['hits']['hits']:
            #print(hit)
            hit['_source']['_score'] = hit['_score']
            arrayList.append(hit['_source'])

        return arrayList 
        
