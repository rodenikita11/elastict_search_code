<style type="text/css"> .image {max-height: 186px;}</style>
<div class="breadcrumb outer-top-150" style="padding-top:10px">
  <div class="container">
    <div class="breadcrumb-inner">
      <ul class="list-inline list-unstyled">
        <li><a href="<?php echo ADMINURL;?>">Home</a></li>
        <li class='active'><a href="<?php echo ADMINURL;?>product/promotion/<?php echo $promotionid; ?>">Offer Zone</a></li>
        <li class='active'><a href="<?php echo ADMINURL;?>product/promotion/<?php echo $promotionid; ?>"><?php echo $promotionproduct['promotionname']; ?></a></li>
      </ul>
    </div>
    <!-- /.breadcrumb-inner -->
  </div>
   <!-- /.container -->
</div>
<div class="body-content ">
   <div class='container inner-bottom-30'>
      <div class='row'>
      <div class='col-md-3'>
        <? include('include/left_sidebar_promotion.php'); ?>
        </div>
        <div id='toTop'>Go To Top!</div>
         <div class='col-md-9'>
            <!-- ========================================== SECTION – HERO ========================================= -->
            <!--<div id="category" class="category-carousel hidden-xs">
               <div class="item">
                  <div class="image"> <img src="<?php echo WEBIMG?>banners/cat-banner-1.jpg" width="100%" alt="" class="img-responsive"> </div>
               </div>
            </div> -->
            <!--======================== Featured products ======================================--> 
            <div class="clearfix filters-container m-t-10">
               <div class="row" style="width: 100%;margin: 0;">
               <?php
    if($param[id]=='927'){		//----privious promotion id=753--------//
      echo'<div><strong><ol>

      Terms and Conditions:- 

<li>All books are sold in as is where is condition. Books may be shopsoiled due to storage. Please note, no returns or exchange on any of the purchases under this offer. </li>
<li>Fulfillment of order depends on stock availability. It may be possible that stock may not be available after you place your order. We shall intimate you in such instances and refund the order value for the book(s) not in stock. </li>
<li>This offer may have the same physical title at differing prices. If there is any difference in price of the book physically available we shall contact you.</li>
<li>No other offer can be clubbed with this offer. </li>
<li>Furtados reserves the right to discontinue this offer at any time at its own discretion.

 </li>
<li>Despatch and postal charges extra as applicable. </li></ol> </strong></div>';

    }
    elseif($param[id]=='906'){
//         echo'<div><strong><ol>

//       Terms and Conditions:- 

// <li>This offer is available on select products only.</li>
// <li>This offer expires at 11:59pm, April 14, 2020. Complete payment for the product needs to be made before expiration of the offer.</li>
// <li>We will refund all money in full if any item ordered sells out and is not available by the time the money is received.</li>
// <li>All orders placed will be shipped after the lock-down is lifted and shipping of products is allowed. As there will be a huge backlog of deliveries across all logistics services, there is a possibility of some delay in customers receiving the purchased products after the lock-down is lifted, but every effort will be made to expeditiously despatch all orders as soon as possible.</li>
// <li>This offer cannot be clubbed with any other existing offers. </li></ol> </strong></div>';

      echo '<div class="image"> <img src="https://www.furtadosonline.com/uploads/images/banner/04-2020/746x189_CAT ABNNER.jpg" width="100%" alt="" class="img-responsive" style="height:186px !important"> </div>';

    }

   ?>
                  <section id="product-tabs-slider" class="section featured-product  fadeInUp">
                      <h1 class="section-title pdl" style="float: left;"><?php echo $promotionproduct['promotionname']; ?></h1>
                        <span style="float: right;margin-top: 14px;margin-bottom: 22px;margin-right: -5px;font-size: 14px;  text-transform: uppercase;">
                            Sort By
                            <select id="prosuctsort_opt" onchange="filterofferall(<?php echo $_GET['id']?>);">
                               <option value="popular">Popularity</option>
                               <option value="highprice">High Price</option>
                               <option value="lowprice">Low Price</option>
                            </select>
                         </span>
                      <div class="col col-sm-12 col-md-12 col-xs-12 inner-right-0">
                        <?php foreach($promotionproduct['product'] as $key => $val){ //print_r($val); 
                               include('include/list_catproductlist.inc'); 
                          }?>
                      </div>
                      <div class="col col-sm-12 col-md-12 col-xs-12 inner-right-0 productpagination">
                          <span><?php echo $promotionproduct['links']; ?></span>
                      </div>
                  </section>
               </div>
            </div>
            <!--======================== Featured products: END ======================================--> 
            
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   function filterofferall(eleid){
      //alert(ele); return false;
     // curURI=window.location.pathname;
      var curURL=jQuery(location).attr('href');
      var cururl1=curURL.split('?');
      var appendstr=(cururl1.length > 1) ? '&' : '?&';
      //urlid = ele;
      //alert(curURL); return false;
      var sort = $( "#prosuctsort_opt option:selected").val();
      if(sort==undefined){sort = 'popular';}
      if(sort){
         window.location.href=curURL+appendstr+"sort="+sort;
      }
   }
</script>