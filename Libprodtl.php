<?php
class Libprodtl
{
    public $ci;
    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->model('Do_product');
        $CI->load->model('Do_common');
        $this->ci = $CI;
        
    }
    
	protected function log_fileDetails($fun_name, $query){
		$this->log = '------------------FileName: Libprodtl.php - Function Name: '.$fun_name.'-----'.date("F j, Y, g:i a").'--------------------'.PHP_EOL;
		$this->log = $this->log . json_encode($query).PHP_EOL; // appending the query
		$this->logpath = APPPATH . 'logs/admin_database_queries-' . date('Y-m-d') . '.log';
		error_log($this->log, 3, $this->logpath);
	}
    public function inert_review($param)
    {
        if ($param) {
            if (is_int($param['id'])) {
                $res = $this->ci->Do_product->insert_review($param);
                
            } else {
                echo "invalid data type of product id";
                exit;
            }
            
        }
        
    }
    
    
    public function getsimiliar_prod($param)
    {
        
        if ($param) {
            if (is_int($param['id'])){
                $memkey='similar_products'.json_encode($param);

                $memCacheDate = $this->ci->libsession->getMemcache($memkey);
                if($memCacheDate){
                    return $memCacheDate;
                }

                $res= $this->ci->Do_product->getsimiliar_prod($param);
                //print_R($res); die;  
                $wishlist = $this->ci->libsession->getSession('wishlist');
                if ($res) {
                    foreach ($res as $row) {
                        //print_r($row);die;
                        if ($wishlist) {
                            $arrwishlist = explode(',', $wishlist);
                            if (in_array($row['proid'], $arrwishlist)) {
                                $prodata[$row['proid']]['wishlist'] = '1';
                            } else {
                                $prodata[$row['proid']]['wishlist'] = '0';
                            }
                        } else {
                            $prodata[$row['proid']]['wishlist'] = '0';
                        }
                        
                        $proname                        = preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
                        $proname                        = str_replace("--", "-", $proname);
                        $proname                        = strtolower(rtrim($proname, '-'));
                        $proid                          = $row['proid'];
                        $prodata[$row['proid']]['id']   = $row['proid'];
                        $prodata[$row['proid']]['url']  = '' . SITEMOBURL . '' . $proname . '/' . $proid . '';
                        $prodata[$row['proid']]['name'] = $row['proname'];
                        
                        if ($row['prothumbnail']) {
                            $prodata[$row['proid']]['img'] = str_replace('http:', 'https:', $row['prothumbnail']);
                        } else {
                            $prodata[$row['proid']]['img'] = SITEIMAGES . 'mobimg/noimage1.png';
                        }
                        
                        $prodata[$row['proid']]['price']         = $row['onlinePrice'];
                        $prodata[$row['proid']]['qty']           = $row['quantity'];
                        $prodata[$row['proid']]['discountprice'] = $row['discountPrice'];
                        $setprice                                = $row['onlinePrice'] - $row['discountPrice'];
                        $prodata[$row['proid']]['percentage']    = round($setprice / $row['onlinePrice'] * 100);
                        $prodata[$row['proid']]['callforprice']  = $row['callforprice'];
                        $prodata[$row['proid']]['isAvailable']   = $row['isAvailable'];
                        
                    }
                    $this->ci->libsession->setMemcache($memkey,$prodata);
                    //print_R($prodata); die;  
                    return $prodata;
                }
            } else {
                echo "invalid data type of product id";
                exit;
            }
            
        }
    }
    
    public function get_offerfilter($param){
        $memkey='get_offerfilter'.json_encode($param);
        //echo $memkey; die;
        $memCacheDate = $this->ci->libsession->getMemcache($memkey);
        if($memCacheDate){
            return $memCacheDate;
        }
        
        $res['filter_offer']= $this->ci->Do_product->getofferfilter($param);
        //echo '<pre>';
        //print_r($res['filter_offer']); die;
        if($res['filter_offer']){
            $i=0;
            foreach($res['filter_offer']['brand'] as $key => $val){
                $filterdata['getfilter']['brand'][$i]['brandid']= $val['brandid'];
                $filterdata['getfilter']['brand'][$i]['brandname']= $val['brandname'];
                $i++;
            }
            $filterdata['getfilter']['available']=$res['filter_offer']['available'][1]['offerid'];
            $filterdata['getfilter']['notavailable']=$res['filter_offer']['available'][0]['offerid'];
            $k= 0;
            foreach ($res['filter_offer']['category'] as $key => $val){
                if($val['catparent_id'] !=1 && $val['catparent_id']!=2 && $val['catparent_id']!=3 && $val['catparent_id']!=4){
                    $url=ADMINURL.'product/promotion/'.$param['id'].'/?categoryid='.$val['categoryid'];
                    $filterdata['filter']['category'][$val['catparentname']]['url']=ADMINURL.str_replace(array(' ', '&'), array('','-'), $val['catparentname']).'/'.$val['catparent_id'];
                    $filterdata['filter']['category'][$val['catparentname']]['sub'][$k]['id']=$val['categoryid'];
                    $filterdata['filter']['category'][$val['catparentname']]['sub'][$k]['name']=$val['catname'];
                    $filterdata['filter']['category'][$val['catparentname']]['sub'][$k]['url']=$url;
                    $k++;
                }
            }
            //echo '<pre>';
            //print_r($filterdata); die;
            $filterdata['getfilter']['minprice']= $res['filter_offer']['price'][0]['minamount'];
            $filterdata['getfilter']['maxprice']= $res['filter_offer']['price'][0]['maxamount'];

            foreach($res['filter_offer']['discount'] as $key=>$val){
                $filterdata['discount'][$val['discount']]=$val['discount'];
            }

            $this->ci->libsession->setMemcache($memkey,$filterdata);
            return $filterdata;
        }
    }
    
    public function getproductpromotiondtl($param){
        if ($param) {
            // print_r($param);
            // $param['id']=intval($param['id']);
            // //print_r($param['id']);
            if (is_int($param['id'])) {
                //$cacheparam['id']=$param['id'];
               // $memkey = 'libprodtl_promotion_'.$cacheparam.'_'.$param['start'];
                //echo $memkey; die;
                //error_log($memkey); 
                //$memCacheDate = $this->ci->libsession->getMemcache($memkey);
               /// if($memCacheDate){
                   // return $memCacheDate;
               // }
                $param['count']=true;
                $rescount=$this->ci->Do_product->getproductpromotiondtl($param);
                $total_rows=$rescount['prodata'][0]['cnt'];
                if($param['bookofthemonth']==1){
                    $config['base_url']= ($param['module']!='brandsfilters') ? ADMINURL.'product/bookofthemonth' : ADMINURL.$param['brandname'].'/'.$param['brandid'];
                }else{
                     $config["base_url"] = ADMINURL.'product/promotion/'.$param['id'];
                }
                $config['total_rows'] =$total_rows;
                $config['per_page'] = LIMIT;
                $config['uri_segment']=4;
                $config['enable_query_strings'] = true;
                if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");

                $this->ci->pagination->initialize($config);
                $param['count']=false;
                $res = $this->ci->Do_product->getproductpromotiondtl($param);
                
                $wishlist = $this->ci->libsession->getSession('wishlist');
                if ($wishlist) {
                    $arrwishlist = explode(',', $wishlist);
                }
                $prodata['links']=$this->ci->pagination->create_links();
                //echo $prodata['links']; die;
                //echo '<pre>';
                //print_r($res['prodata']);die;
				// $this->log_fileDetails('getproductpromotiondtl', $res);
                if ($res['prodata']){
                    $prodata['promotionname']                  = $res['prodata'][0]['promotionname'];
                    foreach ($res['prodata'] as $row) {

                        if ($wishlist) {
                            if (in_array($row['proid'], $arrwishlist)) {
                                $prodata['product'][$row['proid']]['wishlist'] = '1';
                            } else {
                                $prodata['product'][$row['proid']]['wishlist'] = '0';
                            }
                        } else {
                            $prodata['product'][$row['proid']]['wishlist'] = '0';
                        }
                        
                        $proname= preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
                        $proname= str_replace("--", "-", $proname);
                        $proname= strtolower(rtrim($proname, '-'));
                        $proid= $row['proid'];
                        $prodata['product'][$row['proid']]['id']   = $proid;
                        $prodata['product'][$row['proid']]['url']  = '' . SITEMOBURL . '' . $proname . '/' . $proid . '';
                        $prodata['product'][$row['proid']]['name'] = $row['proname'];
                        if ($row['prothumbnail']) {
                            $prodata['product'][$row['proid']]['img'] = str_replace('http:', 'https:', $row['prothumbnail']);
                        } else {
                            $prodata['product'][$row['proid']]['img'] = SITEIMAGES . 'mobimg/noimage1.png';
                        }
                        $prodata['product'][$row['proid']]['promotiontitle']  = $row['title'];
                        $prodata['product'][$row['proid']]['promotionname']   = $row['name'];
                        $prodata['product'][$row['proid']]['promotionbanner'] = str_replace('http:', 'https:', $row['banner']);
                        $prodata['product'][$row['proid']]['logoimg']         = $row['logoimg'];
                        $prodata['product'][$row['proid']]['topMenuImage']    = $row['topMenuImage'];
                        $prodata['product'][$row['proid']]['price']           = $row['onlinePrice'];
                        $prodata['product'][$row['proid']]['qty']             = $row['quantity'];
                        $setprice                                  = $row['onlinePrice'] - $row['discountPrice'];
                        $prodata['product'][$row['proid']]['percentage']      = round($setprice / $row['onlinePrice'] * 100);
                        $prodata['product'][$row['proid']]['callforprice']    = $row['callforprice'];
                        $prodata['product'][$row['proid']]['discountprice']   = $row['discountPrice'];
                        $prodata['product'][$row['proid']]['avaliable']       = $row['isAvailable'];

                        /*$setprice=$row['onlinePrice']-$row['discountPrice'];
                        $prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
                        if($row['callforprice']=='NO'){
                            $percentage=round($setprice/$row['onlinePrice']*100);
                            if($percentage >0 && $percentage <=5){
                                $prodata['percentage']['0_5']='0 to 5%';
                            }else if($percentage >5 && $percentage <=10){
                                $prodata['percentage']['5_10']='5 to 10%';
                            }else if($percentage >10 && $percentage <=15){
                                $prodata['percentage']['10_15']='10 to 15%';
                            }else if($percentage >15 && $percentage <=20){
                                $prodata['percentage']['30_40']='30 to 40%';
                            }else if($percentage >40){
                                $prodata['percentage']['40_more']='40% and more';
                            }
                        }*/
                    }

                    //echo '<pre>';
                    //print_r($prodata); die;

                    $this->ci->libsession->setMemcache($memkey,$prodata);
                    return $prodata;
                }
            } else {
                echo "invalid data type of product id";
                exit;
            }
            
        }
    }
    
    public function getpromotiondtl()
    {
        $res = $this->ci->Do_product->getallpromotions();
        // print_r($res);die;
        if ($res['prodata']) {
            foreach ($res['prodata'] as $row) {
                $prodata[$row['id']]['url']       = '' . SITEMOBURL . 'product/promotion/' . $row['id'] . '';
                $prodata[$row['id']]['title']     = $row['title'];
                $prodata[$row['id']]['img']       = str_replace('http:', 'https:', $row['bannerImage']);
                $prodata[$row['id']]['startDate'] = $row['startDate'];
                $prodata[$row['id']]['endDate']   = $row['endDate'];
                $prodata[$row['id']]['promotionofferDisplay']   = $row['promotionofferDisplay'];
            }
            //print_R($prodata); die; 
            return $prodata;
        }
    }
    
    public function getrecently_viewed($param)
    {
        //print_R($param); die;
        if ($param) {
            if (is_int($param['id'])) {
                // print_R($_COOKIE['proid']);  die;
                
                
                if (isset($_COOKIE['proid'])) {
                    $existedcookie = unserialize($cookieval);
                    print_R($existedcookie);
                    if (!in_array($existedcookie, $param['id'])) {
                        $existedcookie[] = $param['id'];
                        $existedcookie   = $existedcookie;
                        print_R($cookieval);
                        $cookieval = serialize($existedcookie);
                    }
                } else {
                    $cookieval = $param['id'];
                }
                setcookie('proid', $cookieval);
                
            }
        }
        
        
        
    }
    
    public function addreview($param)
    {
        
        if ((int) $param['pid']) {
            $param['pid'] = (int) $param['pid'];
            
            $res = $this->ci->Do_product->insert_review($param);
            
            if ($res) {
                return $res;
            }
        } else {
            echo "there is some problem with the parameter";
        }
        echo json_encode($data);
    }
    
    /*created by viki 28/2/2018*/
    public function addenquirydtl($param)
    {
        if ($param) {
            $param['productId'] = intval($param['productId']);
            if (is_int($param['productId'])) {
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $param['ip'] = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $param['ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $param['ip'] = $_SERVER['REMOTE_ADDR'];
                }
                //print_R($param);
                $res = $this->ci->Do_product->addenquirydtl($param);
                if ($res) {
                    return true;
                } else {
                    return false;
                }
            } else {
                echo "invalid data type of product id";
            }
            echo json_encode($data);
        }
        
    }
    public function pianoenquirydtl($param)
    {
        if ($param) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $param['ip'] = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $param['ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $param['ip'] = $_SERVER['REMOTE_ADDR'];
            }
            $res = $this->ci->Do_product->addpianoenquirydtl($param);
            if ($res) {
                return true;
            } else {
                return false;
            }
            echo json_encode($data);
        }
        
    }
    
    /*end viki 28/2/2018*/
}
?> 