<?php
class Liburl{
             public $ci;
          public function __construct() {
             $CI = & get_instance();
             $CI->load->model('Do_product');
			  $CI->load->model('Do_common');
			  $CI->load->library('pagination');
			  $CI->load->library('libsession');
             $this->ci = $CI;

         }
		private function log_fileDetails_v2($fun_name, $query){
			$this->log = '------------------FileName: Liburl.php - Function Name: '.$fun_name.'-----'.date("F j, Y, g:i a").'--------------------'.PHP_EOL;
			$this->log = $this->log . $query.PHP_EOL; // appending the query
			$this->logpath = APPPATH . 'logs/data_logging-' . date('Y-m-d') . '.log';
			error_log($this->log, 3, $this->logpath);
		}
		 public function getinfotype($param){
			 //var_dump($param);
			 if($param){
				 if(is_int($param['id'])){
					 //die('once');
					 $res =$this->ci->Do_product->getinfotype($param);
					 // $this->log_fileDetails_v2('getinfotype', json_encode($res));
					 return $res;

				 }else{
					 echo "invalid data type of product id"; exit;
				 }

			 }

		 }

		public function get_productdetail($param){

       $memkey = 'liburl_get_productdetail'.json_encode($param);
      //echo $memkey; die;
       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
       if($memCacheDate && false){
         return $memCacheDate;
       }

			if(isset($param['pid'])){
				$param['id']=intval($param['pid']);
			}
           if($param){
			   if(is_int($param['id'])){
				$model['id']=$param['id'];

				if($param['sid']){

					$model['sid']=$param['sid'];
				}
				if(is_string($param['filterdtl'])){
					$model['allattribute']=implode(',',$param['filterdtl']);
                    //print_R($model['allattribute']); die;
				}
			    $res=$this->ci->Do_product->getprodtl($model);
				//print_r($res); die;
				 if($res){
					// print_r($res['prodata']); echo 'vikram';
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $res['prodata'][0]['proname']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));

					$productName = stripslashes($res['prodata'][0]['proname']);
					 $proid=$res['prodata'][0]['proid'];
					 $prodata['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
					 $prodata['proid']=$res['prodata'][0]['proid'];
					// echo 'category path'.explode(',',$res['prodata'][0]['categorypath'])[1];
					 $prodata['third_catid']=explode(',',$res['prodata'][0]['categorypath'])[1];
                     $prodata['name']=$res['prodata'][0]['proname'];
					 $prodata['img']=str_replace('http:', 'https:', $res['prodata'][0]['prothumbnail']);
					 $prodata['embedCode']=$res['prodata'][0]['embedCode'];
					  if($res['prodata'][0]['prothumbnail']){
						  $bigImage = str_replace('_100x75.','_350x350.',$res['prodata'][0]['prothumbnail']);
						 // echo  $bigImage; die;
					 	 $prodata['img']=$this->getprothumbnail($bigImage,DWIMGDTL);
					 }else{
						$prodata['img']=SITEIMAGES.'mobimg/noimage1.png';
					 }
					$wishlist=$this->ci->libsession->getSession('wishlist');
					 if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						 if(in_array($proid,$arrwishlist)){
							 $prodata['wishlist']='1';
						 }else{
							$prodata['wishlist']='0';
						 }
					 }else{
						  $prodata['wishlist']='0';
					 }
					 $prodata['onlinePrice']=$res['prodata'][0]['onlinePrice'];
					 $prodata['qty']=$res['prodata'][0]['quantity'];
					 $prodata['discountPrice']=$res['prodata'][0]['discountPrice'];
					 $setprice=$res['prodata'][0]['onlinePrice']-$res['prodata'][0]['discountPrice'];
					$prodata['discountpercentage']= round($setprice/$res['prodata'][0]['onlinePrice']*100);
					
					$prodata['mfgLocation']=$res['prodata'][0]['mfgLocation'];


					 $prodata['shortspecification']=$res['prodata'][0]['prodescription'];
					 $prodata['longdesc']=$res['prodata'][0]['prolongspecification'];
					 $prodata['brandid']=$res['prodata'][0]['brandid'];
					 //echo '<pre>'; print_r($res['prodata']); die;
					 $prodata['brandname']=$res['prodata'][0]['brandname'];
					 $prodata['brandimg']=str_replace('http:', 'https:', $res['prodata'][0]['brandimage']);
					 $prodata['brandurl']=SITEURL.''.$prodata['brandname'].'/'.$prodata['brandid'];
					 $prodata['branddescription']=$res['prodata'][0]['branddescription'];
					 $prodata['catname']=$res['prodata'][0]['catname'];
					 $prodata['catid']=$res['prodata'][0]['category_id'];
					 $prodata['catparent_id']=$res['prodata'][0]['catparent_id'];
					 $prodata['revcnt']=$res['prodata'][0]['rvcnt'];
					 $prodata['overallrating']=$res['prodata'][0]['overallrating'];
					 $prodata['quantity'] = $res['prodata'][0]['quantity'];
                     $prodata['avaliable'] = $res['prodata'][0]['isAvailable'];
					 $prodata['callforprice'] = $res['prodata'][0]['callforprice'];
					 for($i=0;$i<count($res['proimage']);$i++){
						 $prodata['images'][$i]['pro_img_id']=$res['proimage'][$i]['pro_img_id'];
						 $prodata['images'][$i]['proimagetitle']=$res['proimage'][$i]['proimagetitle'];

						 $image=str_replace('_100x75.','_350x350.',$res['proimage'][$i]['pro_image']);
						 //echo $image; die;
						 $prodata['images'][$i]['pro_image']=str_replace('http:', 'https:', $image);
						 $prodata['images'][$i]['pro_img_sort_order']=$res['proimage'][$i]['pro_img_sort_order'];
					 }
                   //	print_R($prodata['images']); die;
					 $prodata['review']=$res['rewview'];
				 }
				 //print_r($prodata);die;

				if($res['prodata'][0]['isBook']){
					$prodata['meta_title']='Buy '.$this->getPlainText($productName).' Book Online in India | Furtados';
					$prodata['meta_description'] = 'Furtados : Buy '.$this->getPlainText($productName).' Book Online in India at Best Prices.';
					$prodata['meta_keyword']=$this->getPlainText($productName);
				}else{
					$prodata['meta_title']='Buy '.$productName.' Online in India at Best Price | Furtados';
					$prodata['meta_description']='Find the best '.$productName.' Online in India at competitive prices. ✓Best Prices ✓Quick Delivery ✓Quality Assured';
					$prodata['meta_keyword']=$this->getPlainText($productName);
				}

           $this->ci->libsession->setMemcache($memkey,$prodata);
				return $prodata;
			}else{
           echo "invalid data type of product  id"; exit;
         }

		   }

      }

	public function getPlainText($para){
		$old_pattern = array("/[^a-zA-Z0-9. ]/", "/_+/", "/_$/");
		$new_pattern = array("");
		$newPara = preg_replace($old_pattern, $new_pattern , $para);
		$newPara = ucwords(strtolower($newPara));

		return $newPara;
	}

	  public function get_categorydetailsX($param, $paramx){
	  		//echo 'under this';die('mara');
	       $memkey = 'liburl_get_categorydetails'.json_encode($param);

	       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
	       if($memCacheDate && false){
	         return $memCacheDate;
	       }
	       /**** New Elastic Search Code ****/
			// user id check kar... and search id ke sath log kar de!
	       if($param){
			   if(is_int($param['id'])){
			   		if(is_string($param['branddtl'])){
						$param['branddtl']=json_decode(str_replace('_', ',', $param['branddtl']));

					}
					if(is_string($param['search'])){
						$param['search']=$param['search'];

					}
					if(is_string($param['avaliable'])){
						$param['avaliable']=json_decode($param['avaliable']);

					}
					if($param['percentage']){
				 		$param['percentage']=json_decode($param['percentage']);
					}

					$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
					$url = "http://3.210.70.215:8080/listing/api/v1.0/0/general/".$param['start']."/".LIMIT.'/'.rawurlencode($param['id']).'?x=';
					$base_url = "http://3.210.70.215:8080/listing/api/v1.0/0/general/0/0/".rawurlencode($param['id']).'?x=';
					if( isset($paramx['branddtl']) || isset($paramx['available']) || isset($paramx['minprice']) ||
							isset($paramx['maxprice']) || isset($_SESSION['usrid']) || isset($paramx['catid']) || 
							isset($paramx['itemtype']) || isset($paramx['discount'])|| isset($paramx['sort'])
						){
							$url=$url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$paramx['minprice'].$paramx['maxprice'].$paramx['discount'].$paramx['sort'].$usrid;
							$base_url = $base_url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$usrid;
						}
					// var_dump($param, $paramx); 
					// echo $url; exit;
					$get_base_data = $this->ci->libapi->callAPI('GET', $base_url , false);
					$get_data = $this->ci->libapi->callAPI('GET', $url , false);
					//var_dump($url, $get_data); exit;
					$base_res = json_decode($get_base_data, true);
					$res1 = json_decode($get_data, true);
					$errors = $res['res']['errors'];
					

					// $res=$this->ci->Do_product->productsearch($param);$param['avaliable']
					$wishlist=$this->ci->libsession->getSession('wishlist');

					if($res1){
						if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");

						$patterntocheck=array("/[\/\&%#',\"\$\s]/");
						$pattertobereplaced=array("-");
						$namereplacearray=array('--', '---', '----', '-----');
						$filteredname=preg_replace($patterntocheck, $pattertobereplaced, $param['name']);
						$securename=str_replace($namereplacearray, $pattertobereplaced, $filteredname);
						$config["base_url"] = ADMINURL.$securename.'/'.$param['id'];
						$config['total_rows']=$res1['total'];
						$config['per_page']=LIMIT;
						$config['uri_segment']=3;
						$config['enable_query_strings']=TRUE;
						$this->ci->pagination->initialize($config);
						$prodata['links']=$this->ci->pagination->create_links();

						
						$model['count']=false;
						foreach($res1['data'] as $row){
							$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
							$proname=str_replace("--","-", $proname);
							$proname=strtolower(rtrim($proname,'-'));
							$prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$row['proid'].'';
							if($wishlist){
								$arrwishlist=explode(',',$wishlist);
								//print $row['proid'].'hiiiii';
								if(in_array($row['proid'],$arrwishlist)){
									$prodata['product'][$row['proid']]['wishlist']='1';
								}else{
									$prodata['product'][$row['proid']]['wishlist']='0';
								}
							}else{
								$prodata['product'][$row['proid']]['wishlist']='0';
							}
							$prodata['product'][$row['proid']]['id']=$row['proid'];
							$prodata['product'][$row['proid']]['url']=SITEMOBURL.''.$proname.'/'.$row['proid'].'';
							$prodata['product'][$row['proid']]['name']=$row['proname'];
							$prodata['product'][$row['proid']]['catname']=$row['third_catname'];
							$prodata['maincatname']=$row['maincatname'];

							$prodata['product'][$row['proid']]['onlineprice']=$row['onlineprice'];
							$prodata['product'][$row['proid']]['discountprice']=$row['discountprice'];
							$setprice = $row['onlineprice'] - $row['discountprice'];
							//echo $setprice;
							$prodata['product'][$row['proid']]['percentage'] = round($setprice / $row['onlineprice'] * 100);
							// $prodata['product'][$row['proid']]['callforPrice']=$row['callforprice']; -------------------------------> Not Found
							$image1=str_replace(SITEMOBILEURL,FILEBASEPATH,str_replace('http:','https:',$row['prothumbnail']));
							$prodata['product'][$row['proid']]['prothumbnail']= !empty(str_replace('http:','https:',$row['prothumbnail'])) ? str_replace('http:','https:',$row['prothumbnail']) : '';
							$prodata['product'][$row['proid']]['firstcatid']=$row['first_catid'];
							//$prodata[$row['proid']]['image']=$row['prothumbnail'];
							$prodata['product'][$row['proid']]['avaliable']=$row['isavailable'];
							$prodata['product'][$row['proid']]['third_catid']=$row['third_catid'];
							$prodata['product'][$row['proid']]['third_category']=$row['third_catname'];
							$prodata['product'][$row['proid']]['price']=$row['onlineprice'];
					
							// Data not found in search
							 $prodata['catparent_name']=$row['catparentname'];
							 $prodata['catmeta_description']=$row['catmeta_description'];
							 $prodata['catmeta_keyword']=$row['catmeta_keyword'];
							//Added by Pritish 12-06-2018
							$prodata['meta_title']=stripslashes($row['catmeta_title']);
							$prodata['meta_description']=stripslashes($row['catmeta_description']);
							//echo $prodata['meta_description']; die;
							$prodata['meta_keyword']=stripslashes($row['catmeta_keyword']);


							 $prodata['catparentid']=$row['catparentid'];
							 $r1=array(' ','&');
	              			 $r2=array('-','-');
							 $catparurl=str_replace($r1,$r2, $row['catparentname']);
							 $prodata['catparenturl']=ADMINURL.strtolower($catparurl).'/'.$row['catparentid'];
							 if($row['prothumbnail']){
								$prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
							 }else{
								$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
							 }

							 // Extra Data for the products

							 /*$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];

							 $prodata['product'][$row['proid']]['qty']=$row['quantity'];
							 $prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
							 $prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
							 $prodata['product'][$row['proid']]['review']=$row['rvcnt'];
							 $prodata['product'][$row['proid']]['rating']=$row['overallrating'];
							 $prodata['product'][$row['proid']]['brandid']=$row['brandid'];
							 $prodata['product'][$row['proid']]['brandname']=$row['brandname'];
							 $prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
							 $prodata['seo_metadesc']=$row['seo_metadesc'];
							 $prodata['seodescription']=$row['seodescription'];*/

							 if($row['callforprice']=='NO'){
								$percentage=round($setprice/$row['onlinePrice']*100);
								if($percentage >0 && 	$percentage <=10){
									$prodata['percentage']['1_10']='1 to 10%';
								}else if($percentage >10 && 	$percentage <=20){
									$prodata['percentage']['10_20']='10 to 20%';
								}else if($percentage >20 && 	$percentage <=30){
									$prodata['percentage']['20_30']='20 to 30%';
								}else if($percentage >30 && 	$percentage <=40){
									$prodata['percentage']['30_40']='30 to 40%';
								}else if($percentage >40 && 	$percentage <=50){
									$prodata['percentage']['40_50']='40 to 50%';
								}else if($percentage >50 && 	$percentage <=60){
									$prodata['percentage']['50_60']='50 to 60%';
								}else if($percentage >60 && 	$percentage <=70){
									$prodata['percentage']['60_70']='60 to 70%';
								}else if($percentage >70 && 	$percentage <=80){
									$prodata['percentage']['70_80']='70 to 80%';
								}
								else if($percentage >80 && 	$percentage <=90){
									$prodata['percentage']['80_90']='80% to 90%';
								}
								else if($percentage >90 && 	$percentage <=99){
									$prodata['percentage']['90_'.$percentage]='90% to '.$percentage.'%';
								}
							}

						} //For loop ends here
					} // res1 if condition ends here.
					$res2 = $res1=$this->ci->Do_product->getCatBannersFeaturedProduct($param);
					//var_dump($res2['seodescription']);
					if($res2['seodescription'][0]["seodescription"]){
						$prodata['seodescription']=$res2['seodescription'][0]["seodescription"];
						$prodata['meta_keyword']=$res2['seodescription'][0]["catmeta_keyword"];
						$prodata['meta_title']=$res2['seodescription'][0]["catmetatitle"];
						$prodata['meta_description']=$res2['seodescription'][0]["catmeta_description"];
					}
					if($res2['featured_product']){
						//print_r($res1['featured_product']); die;
						 foreach($res2['featured_product'] as $row){
						 	//print_r($row);
							 if($wishlist){
							 $arrwishlist=explode(',',$wishlist);
							if(in_array($row['proid'],$arrwishlist)){
								 $prodata['featured_product'][$row['proid']]['wishlist']='1';
							 }else{
								 $prodata['featured_product'][$row['proid']]['wishlist']='0';
							 }
						 }else{
							  $prodata['featured_product'][$row['proid']]['wishlist']='0';
						 }
						  $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
						 $proname=str_replace("--","-", $proname) ;
						 $proname=strtolower(rtrim($proname,'-'));
						 $proid=$row['proid'];
						 $prodata['featured_product'][$row['proid']]['id']= $proid;
						 $prodata['featured_product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
			           $prodata['featured_product'][$row['proid']]['name']=$row['proname'];
			           $prodata['featured_product'][$row['proid']]['catname']=$row['catname'];
			           if($row['prothumbnail']){
									$prodata['featured_product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
						 }else{
									$prodata['featured_product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
						}
						$prodata['featured_product'][$row['proid']]['price']=$row['onlinePrice'];
						$prodata['featured_product'][$row['proid']]['onlinePrice']=$row['onlinePrice'];
						$setprice=$row['onlinePrice']-$row['discountPrice'];
						$prodata['featured_product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
						$prodata['featured_product'][$row['proid']]['discountprice']=$row['discountPrice'];
						$prodata['featured_product'][$row['proid']]['callforprice']=$row['callforprice'];
						$prodata['featured_product'][$row['proid']]['avaliable']=$row['isAvailable'];
						$prodata['featured_product'][$row['proid']]['qty']=$row['quantity'];

						}
				  }

				  	/* Filters code starts here */
				  	$url_filters = $url."&filters=true";
						// {"third_catid":"1127","third_catname":"WOODWIND ACCESSORIES","first_catid":"2","second_catid":"111","first_catname":"Music Books","second_catname":"MISCELLANEOUS BOOKS"}
					$get_data_filters = $this->ci->libapi->callAPI('GET', $url_filters , false);
					//var_dump($url_filters,$get_data_filters);
					$res_filters = json_decode($get_data_filters, true);
					$cat_len = count($res_filters['data']['unique_cat']);
					// structuring categories to fit UI
					$x=0;
					for( $i=0; $i < $cat_len; $i++ ){
						$newarray['id']=$res_filters['data']['unique_cat'][$i]['third_catid'];
						$newarray['name']=$res_filters['data']['unique_cat'][$i]['third_catname'];

						if($param['module']=='brandsfilters'){
							$newarray['url']=SITEURL.$param['brandname'].'/'.$param['brandid'].'/?maincat=&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
						}else{
							if($param['bookofthemonth']==1){
								$newarray['url']=SITEURL.'product/bookofthemonth/?maincat=&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
							}else{
								$newarray['url']=SITEURL.'product/search/?maincat='.$res_filters['data']['unique_cat'][$i]['first_catid'].'&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
							}
						}

						if($param['module']=='brandsfilters'){
							$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.$param['brandname'].'/'.$param['brandid'].'/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];
						}else{
							if($param['bookofthemonth']==1){
								$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.'product/bookofthemonth/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];
							}else{
								// realx for now
								if(!empty($res_filters['data']['unique_cat'][$i]['first_catname']) && !empty($res_filters['data']['unique_cat'][$i]['second_catname'])){
									$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.'product/search/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];	
								}
							}
						}
						if(!empty($res_filters['data']['unique_cat'][$i]['first_catname']) && !empty($res_filters['data']['unique_cat'][$i]['second_catname'])){
							$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['sub'][] = $newarray;
						}

						$x++;
					}

					// Structuring Brand to fit UI
					$brand_len = count($res_filters['data']['unique_brand']);
					for( $m=0; $m < $brand_len; $m++ ){
						$res_filters['data']['unique_brand'][$m]['brandid'] = $res_filters['data']['unique_brand'][$m]['id'];
					}
					//var_dump($newfilterarray,$res_filters);
					$prodata['brandfilter'] = $res_filters['data']['unique_brand'];
					$prodata['catfilter'] = $newfilterarray;
					$prodata['minprice']= $res_filters['min_amt'];
					$prodata['maxprice']= $res_filters['max_amt'];
					$prodata['mindiscount']= round( $res_filters['min_discperc'] );
					$prodata['maxdiscount']= round( $res_filters['max_discperc'] );
				  /* Get Category Banner from categories */

   				  if($res2['cat_banner']){
					 foreach($res2['cat_banner'] as $row){
						 if($row['imgpath']){
					  		$prodata['cat_banner'][$row['id']]['imgpath']=str_replace('http:', 'https:', $row['imgpath']);
				 		 }else{
					 		$prodata['cat_banner'][$row['id']]['imgpath']=ADMINURL.'/assets/images/cat-banner-1.jpg' ;
						 }
						 $prodata['cat_banner'][$row['id']]['url']=$row['img_link'];
                    	 $prodata['cat_banner'][$row['id']]['sortorder']=$row['img_order'];
			    	}
				  }
				  if($res2['cat_bannerweb']){

					 	//$image1=str_replace(SITEMOBURL,FILEBASEPATH,$res1['cat_bannerweb'][0]['img_path']);
						//echo $image1; die;
						if($res2['cat_bannerweb'][0]['img_path']){
							$prodata['cat_bannerweb']['imgpath']= str_replace('http:', 'https:', $res2['cat_bannerweb'][0]['img_path']);
						}else{
							//$prodata['cat_bannerweb']['imgpath']= ADMINURL.'/assets/web/images/cat-banner.jpg';
						}

					 $prodata['cat_bannerweb']['url']=str_replace('http:', 'https:', $res2['cat_bannerweb'][0]['img_link']);
                     $prodata['cat_bannerweb']['sortorder']=$res2['cat_bannerweb'][0]['img_order'];


				 }else{
					  //$prodata['cat_bannerweb']['imgpath']= ADMINURL.'/assets/web/images/cat-banner.jpg';
					 $prodata['cat_bannerweb']['url']='';
                     $prodata['cat_bannerweb']['sortorder']=1;

				}
				//var_dump($prodata);die('mara');
				$this->ci->libsession->setMemcache($memkey,$prodata);
					 // $this->log_fileDetails_v2('sirus', json_encode($prodata));
				return $prodata;

			}else{

				echo "invalid data type of category id"; exit;

			} // If int param check ends here

		} // if param check ends here
     }

	   public function get_categorydetails($param){
       $memkey = 'liburl_get_categorydetails'.json_encode($param);

       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
       if($memCacheDate && false){
         return $memCacheDate;
       }


        if($param){
			   if(is_int($param['id'])){
				$model['id']=$param['id'];
				$model['count']=$param['count'];
				$model['start']=$param['start'];
				if(is_string($param['filterdtl'])){
					$model['allattribute']=json_decode($param['filterdtl']);

				}
				if(is_string($param['branddtl'])){
					$model['branddtl']=json_decode(str_replace('_', ',', $param['branddtl']));

				}
				if(is_string($param['search'])){
					$model['search']=$param['search'];

				}
				if(is_string($param['avaliable'])){
					$model['avaliable']=json_decode($param['avaliable']);

				}
				if(is_string($param['sort'])){
					$model['sort']=$param['sort'];

				}
				if($param['minprice'] && $param['maxprice']){
					$model['minprice']=$param['minprice'];
					$model['maxprice']=$param['maxprice'];
				}
				if($param['percentage']){
				 	$model['percentage']=json_decode($param['percentage']);
				}
				$res=$this->ci->Do_product->getcatdtl($model);
				//echo '<pre>';
				//print_R($res); die;
				$total_rows=$res['product'][0]['count'];
				//echo $total_rows; die;
					$config = array();
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");

					//echo $total_rows; die;
					$patterntocheck=array("/[\/\&%#',\"\$\s]/");
					$pattertobereplaced=array("-");
					$namereplacearray=array('--', '---', '----', '-----');
					$filteredname=preg_replace($patterntocheck, $pattertobereplaced, $param['name']);
					$securename=str_replace($namereplacearray, $pattertobereplaced, $filteredname);

					$config["base_url"] = ADMINURL.$securename.'/'.$param['id'];
					$config['total_rows'] =$total_rows;
					$config['per_page'] = LIMIT;
					$config['uri_segment']=3;
					$config['enable_query_strings'] = TRUE;
					$this->ci->pagination->initialize($config);
                    
                    $prodata['links']=$this->ci->pagination->create_links();
					$model['count']=false;
					$res1=$this->ci->Do_product->getcatdtl($model);
					$wishlist=$this->ci->libsession->getSession('wishlist');

				 if($res1){
					if($res1['product']){

					 foreach($res1['product'] as $row){
					 	//print '<pre>'; print_r($row);die;
							if($wishlist){
							 $arrwishlist=explode(',',$wishlist);
								if(in_array($row['proid'],$arrwishlist)){
									 $prodata['product'][$row['proid']]['wishlist']='1';
								}else{
									 $prodata['product'][$row['proid']]['wishlist']='0';
								}
							}else{
								  $prodata['product'][$row['proid']]['wishlist']='0';
							}
						$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
						$proname=str_replace("--","-", $proname) ;
						if(substr($proname, 0, 2)!='DB'){
							$proname=strtolower(rtrim($proname,'-'));	
						}else{
							$proname=$proname;
						}
						
						$proid=$row['proid'];
						$prodata['product'][$row['proid']]['id']= $proid;
						// $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
						 $prodata['product'][$row['proid']]['url']=SITEMOBURL.''.$proname.'/'.$proid.'';
						 //echo $prodata['product'][$row['proid']]['url']; die;
	                     $prodata['product'][$row['proid']]['name']=$row['proname'];
	                     $prodata['product'][$row['proid']]['catname']=$row['catname'];
						 $prodata['maincatname']=$row['catname'];
						// $prodata['maincaturl']=$prodata['product'][$row['proid']]['url'];
						 $prodata['catparent_name']=$row['catparentname'];
						 $prodata['catmeta_description']=$row['catmeta_description'];
						 $prodata['catmeta_keyword']=$row['catmeta_keyword'];
						//Added by Pritish 12-06-2018
						$prodata['meta_title']=stripslashes($row['catmeta_title']);
						$prodata['meta_description']=stripslashes($row['catmeta_description']);
						//echo $prodata['meta_description']; die;
						$prodata['meta_keyword']=stripslashes($row['catmeta_keyword']);


						 $prodata['catparentid']=$row['catparentid'];
						 $r1=array(' ','&');
              			 $r2=array('-','-');
						 $catparurl=str_replace($r1,$r2, $row['catparentname']);
						 $prodata['catparenturl']=ADMINURL.strtolower($catparurl).'/'.$row['catparentid'];

							 if($row['prothumbnail']){
								$prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
							 }else{
								$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
							 }

					$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['onlinePrice']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
					if($row['callforprice']=='NO'){
						$percentage=round($setprice/$row['onlinePrice']*100);
						if($percentage >0 && 	$percentage <=10){
							$prodata['percentage']['1_10']='1 to 10%';
						}else if($percentage >10 && 	$percentage <=20){
							$prodata['percentage']['10_20']='10 to 20%';
						}else if($percentage >20 && 	$percentage <=30){
							$prodata['percentage']['20_30']='20 to 30%';
						}else if($percentage >30 && 	$percentage <=40){
							$prodata['percentage']['30_40']='30 to 40%';
						}else if($percentage >40 && 	$percentage <=50){
							$prodata['percentage']['40_50']='40 to 50%';
						}else if($percentage >50 && 	$percentage <=60){
							$prodata['percentage']['50_60']='50 to 60%';
						}else if($percentage >60 && 	$percentage <=70){
							$prodata['percentage']['60_70']='60 to 70%';
						}else if($percentage >70 && 	$percentage <=80){
							$prodata['percentage']['70_80']='70 to 80%';
						}
						else if($percentage >80 && 	$percentage <=90){
							$prodata['percentage']['80_90']='80% to 90%';
						}
						else if($percentage >90 && 	$percentage <=99){
							$prodata['percentage']['90_'.$percentage]='90% to '.$percentage.'%';
						}
					}

					$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];

					 $prodata['product'][$row['proid']]['qty']=$row['quantity'];
					 $prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
					 $prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
					 $prodata['product'][$row['proid']]['review']=$row['rvcnt'];
					 $prodata['product'][$row['proid']]['rating']=$row['overallrating'];
					 $prodata['product'][$row['proid']]['brandid']=$row['brandid'];
					 $prodata['product'][$row['proid']]['brandname']=$row['brandname'];
					 $prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
					 $prodata['seo_metadesc']=$row['seo_metadesc'];
					 $prodata['seodescription']=$row['seodescription'];

					}
				   }
					if($res1['featured_product']){
						//print_r($res1['featured_product']); die;
					 foreach($res1['featured_product'] as $row){
					 	//print_r($row);
						 if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						if(in_array($row['proid'],$arrwishlist)){
							 $prodata['featured_product'][$row['proid']]['wishlist']='1';
						 }else{
							 $prodata['featured_product'][$row['proid']]['wishlist']='0';
						 }
					 }else{
						  $prodata['featured_product'][$row['proid']]['wishlist']='0';
					 }
					  $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['featured_product'][$row['proid']]['id']= $proid;
					 $prodata['featured_product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
		           $prodata['featured_product'][$row['proid']]['name']=$row['proname'];
		           $prodata['featured_product'][$row['proid']]['catname']=$row['catname'];
		           if($row['prothumbnail']){
								$prodata['featured_product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
					 }else{
								$prodata['featured_product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
					}
					$prodata['featured_product'][$row['proid']]['price']=$row['onlinePrice'];
					$prodata['featured_product'][$row['proid']]['onlinePrice']=$row['onlinePrice'];
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['featured_product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
					$prodata['featured_product'][$row['proid']]['discountprice']=$row['discountPrice'];
					$prodata['featured_product'][$row['proid']]['callforprice']=$row['callforprice'];
					$prodata['featured_product'][$row['proid']]['avaliable']=$row['isAvailable'];
					$prodata['featured_product'][$row['proid']]['qty']=$row['quantity'];

					}
				  }
					if($res1['cat_banner']){
					 foreach($res1['cat_banner'] as $row){
						 if($row['imgpath']){
					  $prodata['cat_banner'][$row['id']]['imgpath']=str_replace('http:', 'https:', $row['imgpath']);
				  }else{
					 $prodata['cat_banner'][$row['id']]['imgpath']=ADMINURL.'/assets/images/cat-banner-1.jpg' ;
					 }
					 $prodata['cat_banner'][$row['id']]['url']=$row['img_link'];
                     $prodata['cat_banner'][$row['id']]['sortorder']=$row['img_order'];

			    	}
				  }
					if($res1['cat_bannerweb']){

					 	//$image1=str_replace(SITEMOBURL,FILEBASEPATH,$res1['cat_bannerweb'][0]['img_path']);
						//echo $image1; die;
						if($res1['cat_bannerweb'][0]['img_path']){
							$prodata['cat_bannerweb']['imgpath']= str_replace('http:', 'https:', $res1['cat_bannerweb'][0]['img_path']);
						}else{
							//$prodata['cat_bannerweb']['imgpath']= ADMINURL.'/assets/web/images/cat-banner.jpg';
						}

					 $prodata['cat_bannerweb']['url']=str_replace('http:', 'https:', $res1['cat_bannerweb'][0]['img_link']);
                     $prodata['cat_bannerweb']['sortorder']=$res1['cat_bannerweb'][0]['img_order'];


				 }else{
					  //$prodata['cat_bannerweb']['imgpath']= ADMINURL.'/assets/web/images/cat-banner.jpg';
					 $prodata['cat_bannerweb']['url']='';
                     $prodata['cat_bannerweb']['sortorder']=1;



					 }

					 $this->ci->libsession->setMemcache($memkey,$prodata);
					 // $this->log_fileDetails_v2('sirus', json_encode($prodata));
				   return $prodata;

			   }else{


					 echo "invalid data type of category id"; exit;

			   }

		   }

      }
     }

	  public function getbranddtl($param){
		    if($param){
			   if(is_int($param['id'])){
				$model['id']=$param['id'];
				$model['count']=$param['count'];
				$model['start']=$param['start'];
				if(is_string($param['filterdtl'])){
					$model['allattribute']=json_decode($param['filterdtl']);

				}
			     $res=$this->ci->Do_product->getbrnaddtl($model);

				 $total_rows=$res[0]['count'];

					$config = array();
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");



					$config['total_rows'] =$total_rows ;
					$config['per_page'] = LIMIT;
					$config['uri_segment'] = 4;
					$this->ci->pagination->initialize($config);
                    $prodata['links']=$this->ci->pagination->create_links();
					 $model['count']=false;
					 $res1=$this->ci->Do_product->getbrnaddtl($model);

				 if($res1){

					 foreach($res1 as $row){

					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['product'][$row['proid']]['id']= $proid;
					 $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
                     $prodata['product'][$row['proid']]['name']=$row['proname'];
					 if($row['prothumbnail']){
					 $prodata['product'][$row['proid']]['img']=str_replace('http:', 'https:', $row['prothumbnail']);
					 }else{
						$prodata['product'][$row['proid']]['img']=IMAGEDEFAULT;
					 }

					$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];

					$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];


					 $prodata['product'][$row['proid']]['qty']=$row['quantity'];
					 $prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
					 $prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
					 $prodata['product'][$row['proid']]['review']=$row['rvcnt'];
					 $prodata['product'][$row['proid']]['rating']=$row['overallrating'];
					 $prodata['product'][$row['proid']]['brandid']=$row['brandid'];
					 $prodata['product'][$row['proid']]['brandname']=$row['brandname'];

					}


				 }
				  return $prodata;
			   }else{

					 echo "invalid data type of category id"; exit;

			   }

		   }


	  }

        public function getfilter($param){

          $memkey = 'liburl_getfilter'.json_encode($param);
          $memCacheDate = $this->ci->libsession->getMemcache($memkey);
          if($memCacheDate){
            return $memCacheDate;
          }

			if($param){
				  if(is_int($param['cid']) ){
					  $param['cid']=$param['cid'];
					  $result=$this->ci->Do_common->getfilter($param);

				      $resprice=$this->ci->Do_common->getpricerange($param);
				      //echo '<pre>';
				      //print_r($resprice); die;
				      $data['respercentage']=$this->ci->Do_common->getpercentage($param);
				     //print '<pre>'; print_r($resatt); die;
					  $resatt=$this->ci->Do_common->getattribute($param);

						foreach($result as  $row){
							$data['brand'][$row['brandid']]['id' ]=$row['brandid'];
							$data['brand'][$row['brandid']]['name' ]=$row['brandname'];
							$data['brand'][$row['brandid']]['probrandcnt' ]=$row['probrandcnt'];
						}

						foreach($result as $row1){
						 	$data['avalibale']['cnt']=	$row1['proavailablecnt'];
						 	$data['notavaliable']['cnt']=	$row1['pronotavailablecnt'];
						  	$data['procnt']['cnt']=$row['procnt'];
						}
						//print_r($resatt); die;
						foreach($resatt as $rowatt){
							$data['attgrp'][$rowatt['attrigrpid']]['id']=$rowatt['attrigrpid'];
                            $data['attgrp'][$rowatt['attrigrpid']]['name']=$rowatt['attrigrpname'];
						    $data['attgrp'][$rowatt['attrigrpid']]['att'][$rowatt['attriid']]['id']=$rowatt['attriid'];
							$data['attgrp'][$rowatt['attrigrpid']]['att'][$rowatt['attriid']]['attname']=$rowatt['attriname'];
						}

						$data['minprice']=($resprice[0]['minprice']) ? $resprice[0]['minprice'] : 0;
						$data['maxprice']=($resprice[0]['maxprice']) ? $resprice[0]['maxprice'] : 0;

            		$this->ci->libsession->setMemcache($memkey,$data);

					  return $data;
				}else{
					 echo "invalid data type of attribute id"; exit;
				}
			}



		}

		public function getprothumbnail($imgpath,$size){
			if($imgpath && $size){
				return THUMBPATH.'?img='.$imgpath.'&w='.$size;
			}else{
				echo "please specify the size and path for the image"; exit;
			}
		}

	     public function pro_clerance_dtl($param){
			//print_r($param);
           if($param){
			   if(is_int($param['id'])){
				$model['id']=$param['id'];
				if($param['sid']){

					$model['sid']=$param['sid'];
				}
				if(is_string($param['filterdtl'])){
					$model['allattribute']=implode(',',$param['filterdtl']);
                    //print_R($model['allattribute']); die;
				}
			     $res=$this->ci->Do_product->pro_clerance_dtl($model);
			     //print_r($res); echo 'hiii';
				$wishlist=$this->ci->libsession->getSession('wishlist');
				 if($res){
					 if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						if(in_array($res[0]['productId'],$arrwishlist)){
							 $prodata['product'][$res[0]['productId']]['wishlist']='1';
						 }else{
							 $prodata['product'][$res[0]['productId']]['wishlist']='0';
						 }
					 }else{
						  $prodata['product'][$res[0]['productId']]['wishlist']='0';
					 }
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $res[0]['productName']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$res[0]['productId'];
					 $prodata['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
					 $prodata['proid']=$res[0]['proid'];
                     $prodata['name']=$res[0]['productName'];
					// $prodata['img']=$res[0]['productImage1'];
                      $pathchk=FILEBASEPATH.'/uploads/clerance/'.$res[0]['productImage1'];
					  // print_r($pathchk); die('maram');
					  if($res[0]['productImage1']){
						  if(file_exists($pathchk)){
							$prodata['img']=GENERALURL.'uploads/clerance/'.$res[0]['productImage1'];
						  }else{
							$prodata['img']=SITEIMAGES.'mobimg/noimage1.png';
						 }
					 }else{
							$prodata['img']=SITEIMAGES.'mobimg/noimage1.png';
						 }

					 $setprice=$res[0]['onlinePrice']-$res[0]['clearanceMRP'];
					 $devideprice=$setprice/$res[0]['onlinePrice'];
					 $prodata['discountpercentage']= round($devideprice*100);
					 $prodata['onlinePrice']=$res[0]['onlinePrice'];
					 $prodata['qty']=$res[0]['quantity'];
					 $prodata['discountPrice']=$res[0]['clearanceMRP'];

					 $prodata['shortspecification']=$res[0]['proshortspecification'];
					 $prodata['longdesc']=$res[0]['prolongspecification'];
					 $prodata['brandid']=$res[0]['brandid'];
					 $prodata['brandname']=$res[0]['brandname'];
					 $prodata['brandimg']=$res[0]['brandimage'];
					 $prodata['catname']=$res[0]['catname'];
					 $prodata['catid']=$res[0]['category_id'];
					 $prodata['catparent_id']=$res[0]['catparent_id'];
					 $prodata['revcnt']=$res[0]['rvcnt'];
					 $prodata['overallrating']=$res[0]['overallrating'];
					 $prodata['quantity'] = $res[0]['quantity'];
					 $prodata['clearanceId'] = $res[0]['clearanceId'];
					 $prodata['serialid']=$res[0]['serialid'];
					 if($res[0]['clearanceAvailable']==1){
						 $prodata['avaliable'] =1;
					 }else{
						 $prodata['avaliable'] =0;
					 }
						  $pathchk=FILEBASEPATH.'/uploads/clerance/'.$res[0]['productImage1'];
                      if($res[0]['productImage1']){
						 if(file_exists($pathchk)){
						 $prodata['images'][1]['pro_image']=GENERALURL.'uploads/clerance/'.$res[0]['productImage1'];
						$prodata['images'][1]['pro_image']=str_replace('_80x60','_250x190',$prodata['images'][1]['pro_image']);
						  }else{
							 $prodata['images'][1]['pro_image']=SITEIMAGES.'mobimg/noimage1.png';
						  }
					  }else{
							 $prodata['images'][1]['pro_image']=SITEIMAGES.'mobimg/noimage1.png';
						  }
						  $pathchk1=FILEBASEPATH.'/uploads/clerance/'.$res[0]['productImage2'];
						  //echo 		$res[0]['productImage3'];
					  if($res[0]['productImage2']){
						  if(file_exists($pathchk1)){
							$prodata['images'][2]['pro_image']=GENERALURL.'uploads/clerance/'.$res[0]['productImage2'];
							$prodata['images'][2]['pro_image']=str_replace('_80x60','_250x190',$prodata['images'][2]['pro_image']);
						  }	else{
							 $prodata['images'][2]['pro_image']=SITEIMAGES.'mobimg/noimage1.png';
						  }
					  }else{
							 $prodata['images'][2]['pro_image']=SITEIMAGES.'mobimg/noimage1.png';
						  }

					 $prodata['review']=$res['rewview'];
				 }
				#print_R($prodata); die;
				   return $prodata;
			   }else{


					 echo "invalid data type of product  id"; exit;

			   }

		   }

      }

	  public function getsubcat($param){
      $memkey = 'liburl_getsubcat'.json_encode($param);
      $memCacheDate = $this->ci->libsession->getMemcache($memkey);
      if($memCacheDate){
        return $memCacheDate;
      }
		  if($param['cid']){
			  $res=$this->ci->Do_common->getsubcat($param);
			        if($res){
						foreach($res as $row){
						 $catname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['catname']);
						 $catname=str_replace(array('--', ','),"-", $catname) ;
						 $catname=strtolower(rtrim($catname,'-'));
						 $prodata['category'][$row['catid']]['id']= $row['catid'];
						 //$prodata['category'][$row['catid']]['url']=''.SITEURL.''.$catname.'/'.$row['catid'].'';
						 $prodata['category'][$row['catid']]['url']=''.SITEMOBURL.''.$catname.'/'.$row['catid'].'';
						 $prodata['category'][$row['catid']]['name']=$row['catname'];
						}
					}

		  }
      $this->ci->libsession->setMemcache($memkey,$prodata);
		  return  $prodata;

	  }
	  /*viki 19/2/2018*/
	  public function getcat($param){

      $memkey = 'liburl_getcat'.json_encode($param);
      $memCacheDate = $this->ci->libsession->getMemcache($memkey);
      if($memCacheDate){
        	return $memCacheDate;
      }


		  if($param['cid']){
			  $res=$this->ci->Do_common->getcat($param);

			        if($res){
						foreach($res as $row){
						 $subcat=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['subcat']);
						 $maincat=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['maincat']);
						 $subcat=str_replace(array('--', ','),"-", $subcat) ;
						 $maincat=str_replace(array('--', ','),"-", $maincat) ;
						 $subcat=strtolower(rtrim($subcat,'-'));
						 $maincat=strtolower(rtrim($maincat,'-'));
						 $prodata['category'][$row['catid']]['id']= $row['catid'];
						 //$prodata['category'][$row['catid']]['url']=''.SITEURL.''.$catname.'/'.$row['catid'].'';
						 $prodata['category'][$row['catid']]['url']=''.SITEMOBURL.''.$catname.'/'.$row['catid'].'';
						 $prodata['category'][$row['catid']]['subcat']=$row['subcat'];
						 $prodata['category'][$row['catid']]['maincat']=$row['maincat'];
						}
					}

		  }
      $this->ci->libsession->setMemcache($memkey,$prodata);
		  return  $prodata;

	  }

	  public function addbannercount($param){
		  if($param){
		  	//print_r($param); die;
			  $res=$this->ci->Do_common->addbannercount($param);
			  if($res){
				  return true;
			  }else{

				  return false;
			  }
			}


	  }

	   public function getstaticdata($param){
		   $res=$this->ci->Do_common->getstaticdata($param);
			  if($res){
				  return $res;
			  }else{

				  return false;
			  }

	   }

	  /*viki 07/04/2018*/

		public function getjobdetails() {
			$res = $this->ci->Do_common->getjobdetails();
			//print_r($res); die('hisss');
				if($res) {
					return $res;
				} else {
					return false;
				}
		}

		public function brandsmeta($param){
			$res = $this->ci->Do_common->brandsmeta($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getfilterbrand($param){
			$memkey = 'liburl_getfilterbrand'.json_encode($param);
          	$memCacheDate = $this->ci->libsession->getMemcache($memkey);
          	if($memCacheDate){
            	return $memCacheDate;
          	}

			$res=$this->ci->Do_common->getfilterbrand($param);
			$rescount=$this->ci->Do_common->getfilterbrandcount($param);
			if($res){
				$brands=array();
				foreach($res as $val){
					$brands[$val['brandid']]['id']=$val['brandid'];
					$brands[$val['brandid']]['name']=$val['brandname'];
					foreach($rescount as $brandsval){
						if($val['brandid']==$brandsval['brandid']){
							$brands[$val['brandid']]['probrandcnt']=$brandsval['probrandcnt'];
						}
					}
				}
				$this->ci->libsession->setMemcache($memkey,$brands);
				return $brands;
			} else {
				return false;
			}
		}

		public function getfilteravailable($param){
			$res=$this->ci->Do_common->getfilteravailable($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getfilternotavailable($param){
			$res=$this->ci->Do_common->getfilternotavailable($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getfilterprocnt($param){
			$res=$this->ci->Do_common->getfilterprocnt($param);
			if($res) {
				return $res;
			} else {
				return false;
			}	
		}

		public function getfilterminprice($param){
			$res=$this->ci->Do_common->getfilterminprice($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getfiltermaxprice($param){
			$res=$this->ci->Do_common->getfiltermaxprice($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getbrands(){
			$res=$this->ci->Do_common->getbrands();
			if($res){
				return $res;
			}else{
				return false;
			}
		}
	}